import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { BlogpostsService } from './blogposts.service';

@Injectable({
  providedIn: 'root'
})
export class CoastGuardsGuard implements CanActivate {
 
  constructor(private blogposts:BlogpostsService) {}
  
  canActivate(
    ){
    return this.blogposts.Loggedin;
  }
  
}
