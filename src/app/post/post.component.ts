import { Component, OnInit } from '@angular/core';
import { BlogpostsService } from '../blogposts.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  id=0;
  titel?:string;
  imgUrl?:string;
  tekst?:string[];
  tags?:string[]
  


  constructor(private blogposts:BlogpostsService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
      let post = this.blogposts.loadSpecificPost(this.id);
      this.titel = post!.title;
      this.imgUrl = post!.img;
      this.tekst = post!.text;
      this.tags = post!.tags;

    })
  }

}
