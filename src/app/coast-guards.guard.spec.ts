import { TestBed } from '@angular/core/testing';

import { CoastGuardsGuard } from './coast-guards.guard';

describe('CoastGuardsGuard', () => {
  let guard: CoastGuardsGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CoastGuardsGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
