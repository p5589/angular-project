import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BlogpostsService } from '../blogposts.service';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {

  id = 0;
  title = "";
  intro = "";
  text:string[] = [];
  tags:string[] = [];
  isCreated = false;

  constructor(private route:ActivatedRoute, public blogposts:BlogpostsService, public router:Router) { }

  ngOnInit(): void {
  }

  add(){
    let tags:string[] = [];
    if((document.getElementById("board") as HTMLInputElement).checked){
      tags.push("board");
    }
    
    if((document.getElementById("cards") as HTMLInputElement).checked){
      tags.push("cards");
    }
    
    if((document.getElementById("video") as HTMLInputElement).checked){
      tags.push("video")
    }
    
    if((document.getElementById("luck") as HTMLInputElement).checked){
      tags.push("luck")
    }

    if((document.getElementById("strategy") as HTMLInputElement).checked){
      tags.push("strategy")
    }
    this.blogposts.addPost(
      (document.getElementById("title") as HTMLInputElement).value,
      (document.getElementById("intro") as HTMLInputElement).value,
      (document.getElementById("text") as HTMLInputElement).value,
      tags
    )
    this.router.navigate(['/post'],
    { queryParams: { id: this.blogposts.blogposts.length } });
  }
}
