import { getAttrsForDirectiveMatching } from '@angular/compiler/src/render3/view/util';
import { Injectable } from '@angular/core';
import { isTemplateExpression } from 'typescript';

@Injectable({
  providedIn: 'root'
})
export class BlogpostsService {

  searchterm: string ="";
  searchArray?: string[];
  Loggedin: boolean= false;
  username = "admin";
  password = "pas";

  
  
  blogposts = [
    {
      id:1,
      visible:true,
      title:"Monopoly",
      img:"../assets/img/monopoly.jpg",
      tags:["luck", "strategy", "board"],
      intro:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      text:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed bibendum lorem. Duis elementum id tortor et commodo. Quisque in arcu maximus, placerat lorem at, tristique justo. Vivamus a diam tristique, scelerisque dui eu, scelerisque mauris. In cursus pellentesque elit a tempor. Nullam maximus tincidunt tortor in finibus. Quisque eleifend, augue nec fermentum imperdiet, orci nisi interdum neque, non cursus quam orci eu eros. Aenean id bibendum enim.", "Donec elementum, nibh et lobortis dapibus, ex dui auctor magna, eu tempor justo nibh ac neque. Duis fermentum, arcu vel vestibulum tincidunt, odio libero maximus quam, eget interdum erat sapien vel erat. Etiam id nisi tellus. Sed vel fermentum nulla. Mauris sagittis ornare eros non cursus. Mauris pretium ligula libero, eleifend cursus risus aliquam consequat. Pellentesque vestibulum, augue vel consequat mattis, lacus massa rutrum augue, ut venenatis nunc nulla id enim. Etiam id malesuada nibh, non porta tellus. Vivamus viverra porta tortor. In sit amet molestie arcu, ut sodales magna. Nullam in nibh velit. Phasellus malesuada est nec nisl porta ultrices. Nam vulputate ex blandit cursus vehicula.", "Aenean non sapien mi. Aliquam dui tellus, condimentum non enim convallis, maximus viverra quam. Quisque facilisis nunc turpis, nec accumsan arcu tincidunt ut. Nulla commodo massa eget urna viverra, dapibus sagittis lacus accumsan. Ut accumsan viverra tellus et rhoncus. Ut ut cursus urna. Pellentesque ultrices, neque quis dictum faucibus, quam justo blandit odio, quis placerat ligula nisl ut diam. Nam fringilla velit sapien, vitae rhoncus metus rhoncus eget. Nullam non ante sit amet tortor pharetra viverra. Aliquam non tellus mi. Nulla euismod fermentum ligula eget molestie. Mauris vulputate mollis magna eget egestas. Maecenas imperdiet sit amet dui quis condimentum."]
    },
    {
      id:2,
      visible:true,
      title:"Catan",
      img:"../assets/img/catan.jpg",
      tags:["luck", "strategy", "board"],
      intro:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      text:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed bibendum lorem. Duis elementum id tortor et commodo. Quisque in arcu maximus, placerat lorem at, tristique justo. Vivamus a diam tristique, scelerisque dui eu, scelerisque mauris. In cursus pellentesque elit a tempor. Nullam maximus tincidunt tortor in finibus. Quisque eleifend, augue nec fermentum imperdiet, orci nisi interdum neque, non cursus quam orci eu eros. Aenean id bibendum enim.", "Donec elementum, nibh et lobortis dapibus, ex dui auctor magna, eu tempor justo nibh ac neque. Duis fermentum, arcu vel vestibulum tincidunt, odio libero maximus quam, eget interdum erat sapien vel erat. Etiam id nisi tellus. Sed vel fermentum nulla. Mauris sagittis ornare eros non cursus. Mauris pretium ligula libero, eleifend cursus risus aliquam consequat. Pellentesque vestibulum, augue vel consequat mattis, lacus massa rutrum augue, ut venenatis nunc nulla id enim. Etiam id malesuada nibh, non porta tellus. Vivamus viverra porta tortor. In sit amet molestie arcu, ut sodales magna. Nullam in nibh velit. Phasellus malesuada est nec nisl porta ultrices. Nam vulputate ex blandit cursus vehicula.", "Aenean non sapien mi. Aliquam dui tellus, condimentum non enim convallis, maximus viverra quam. Quisque facilisis nunc turpis, nec accumsan arcu tincidunt ut. Nulla commodo massa eget urna viverra, dapibus sagittis lacus accumsan. Ut accumsan viverra tellus et rhoncus. Ut ut cursus urna. Pellentesque ultrices, neque quis dictum faucibus, quam justo blandit odio, quis placerat ligula nisl ut diam. Nam fringilla velit sapien, vitae rhoncus metus rhoncus eget. Nullam non ante sit amet tortor pharetra viverra. Aliquam non tellus mi. Nulla euismod fermentum ligula eget molestie. Mauris vulputate mollis magna eget egestas. Maecenas imperdiet sit amet dui quis condimentum."]
    },
    {
      id:3,
      visible:true,
      title:"Stratego",
      img:"../assets/img/stratego.jpg",
      tags:["strategy", "board"],
      intro:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      text:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed bibendum lorem. Duis elementum id tortor et commodo. Quisque in arcu maximus, placerat lorem at, tristique justo. Vivamus a diam tristique, scelerisque dui eu, scelerisque mauris. In cursus pellentesque elit a tempor. Nullam maximus tincidunt tortor in finibus. Quisque eleifend, augue nec fermentum imperdiet, orci nisi interdum neque, non cursus quam orci eu eros. Aenean id bibendum enim.", "Donec elementum, nibh et lobortis dapibus, ex dui auctor magna, eu tempor justo nibh ac neque. Duis fermentum, arcu vel vestibulum tincidunt, odio libero maximus quam, eget interdum erat sapien vel erat. Etiam id nisi tellus. Sed vel fermentum nulla. Mauris sagittis ornare eros non cursus. Mauris pretium ligula libero, eleifend cursus risus aliquam consequat. Pellentesque vestibulum, augue vel consequat mattis, lacus massa rutrum augue, ut venenatis nunc nulla id enim. Etiam id malesuada nibh, non porta tellus. Vivamus viverra porta tortor. In sit amet molestie arcu, ut sodales magna. Nullam in nibh velit. Phasellus malesuada est nec nisl porta ultrices. Nam vulputate ex blandit cursus vehicula.", "Aenean non sapien mi. Aliquam dui tellus, condimentum non enim convallis, maximus viverra quam. Quisque facilisis nunc turpis, nec accumsan arcu tincidunt ut. Nulla commodo massa eget urna viverra, dapibus sagittis lacus accumsan. Ut accumsan viverra tellus et rhoncus. Ut ut cursus urna. Pellentesque ultrices, neque quis dictum faucibus, quam justo blandit odio, quis placerat ligula nisl ut diam. Nam fringilla velit sapien, vitae rhoncus metus rhoncus eget. Nullam non ante sit amet tortor pharetra viverra. Aliquam non tellus mi. Nulla euismod fermentum ligula eget molestie. Mauris vulputate mollis magna eget egestas. Maecenas imperdiet sit amet dui quis condimentum."]
    },
    {
      id:4,
      visible:true,
      title:"Zeeslag",
      img:"../assets/img/Azul.png",
      tags:["strategy", "board"],
      intro:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      text:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed bibendum lorem. Duis elementum id tortor et commodo. Quisque in arcu maximus, placerat lorem at, tristique justo. Vivamus a diam tristique, scelerisque dui eu, scelerisque mauris. In cursus pellentesque elit a tempor. Nullam maximus tincidunt tortor in finibus. Quisque eleifend, augue nec fermentum imperdiet, orci nisi interdum neque, non cursus quam orci eu eros. Aenean id bibendum enim.", "Donec elementum, nibh et lobortis dapibus, ex dui auctor magna, eu tempor justo nibh ac neque. Duis fermentum, arcu vel vestibulum tincidunt, odio libero maximus quam, eget interdum erat sapien vel erat. Etiam id nisi tellus. Sed vel fermentum nulla. Mauris sagittis ornare eros non cursus. Mauris pretium ligula libero, eleifend cursus risus aliquam consequat. Pellentesque vestibulum, augue vel consequat mattis, lacus massa rutrum augue, ut venenatis nunc nulla id enim. Etiam id malesuada nibh, non porta tellus. Vivamus viverra porta tortor. In sit amet molestie arcu, ut sodales magna. Nullam in nibh velit. Phasellus malesuada est nec nisl porta ultrices. Nam vulputate ex blandit cursus vehicula.", "Aenean non sapien mi. Aliquam dui tellus, condimentum non enim convallis, maximus viverra quam. Quisque facilisis nunc turpis, nec accumsan arcu tincidunt ut. Nulla commodo massa eget urna viverra, dapibus sagittis lacus accumsan. Ut accumsan viverra tellus et rhoncus. Ut ut cursus urna. Pellentesque ultrices, neque quis dictum faucibus, quam justo blandit odio, quis placerat ligula nisl ut diam. Nam fringilla velit sapien, vitae rhoncus metus rhoncus eget. Nullam non ante sit amet tortor pharetra viverra. Aliquam non tellus mi. Nulla euismod fermentum ligula eget molestie. Mauris vulputate mollis magna eget egestas. Maecenas imperdiet sit amet dui quis condimentum."]
    },
    {
      id:5,
      visible:true,
      title:"Nemesis",
      img:"../assets/img/Munchkin.png",
      tags:["strategy", "board"],
      intro:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      text:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed bibendum lorem. Duis elementum id tortor et commodo. Quisque in arcu maximus, placerat lorem at, tristique justo. Vivamus a diam tristique, scelerisque dui eu, scelerisque mauris. In cursus pellentesque elit a tempor. Nullam maximus tincidunt tortor in finibus. Quisque eleifend, augue nec fermentum imperdiet, orci nisi interdum neque, non cursus quam orci eu eros. Aenean id bibendum enim.", "Donec elementum, nibh et lobortis dapibus, ex dui auctor magna, eu tempor justo nibh ac neque. Duis fermentum, arcu vel vestibulum tincidunt, odio libero maximus quam, eget interdum erat sapien vel erat. Etiam id nisi tellus. Sed vel fermentum nulla. Mauris sagittis ornare eros non cursus. Mauris pretium ligula libero, eleifend cursus risus aliquam consequat. Pellentesque vestibulum, augue vel consequat mattis, lacus massa rutrum augue, ut venenatis nunc nulla id enim. Etiam id malesuada nibh, non porta tellus. Vivamus viverra porta tortor. In sit amet molestie arcu, ut sodales magna. Nullam in nibh velit. Phasellus malesuada est nec nisl porta ultrices. Nam vulputate ex blandit cursus vehicula.", "Aenean non sapien mi. Aliquam dui tellus, condimentum non enim convallis, maximus viverra quam. Quisque facilisis nunc turpis, nec accumsan arcu tincidunt ut. Nulla commodo massa eget urna viverra, dapibus sagittis lacus accumsan. Ut accumsan viverra tellus et rhoncus. Ut ut cursus urna. Pellentesque ultrices, neque quis dictum faucibus, quam justo blandit odio, quis placerat ligula nisl ut diam. Nam fringilla velit sapien, vitae rhoncus metus rhoncus eget. Nullam non ante sit amet tortor pharetra viverra. Aliquam non tellus mi. Nulla euismod fermentum ligula eget molestie. Mauris vulputate mollis magna eget egestas. Maecenas imperdiet sit amet dui quis condimentum."]
    },
    {
      id:6,
      visible:true,
      title:"Dammen",
      img:"../assets/img/Azul.png",
      tags:["strategy", "board"],
      intro:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      text:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed bibendum lorem. Duis elementum id tortor et commodo. Quisque in arcu maximus, placerat lorem at, tristique justo. Vivamus a diam tristique, scelerisque dui eu, scelerisque mauris. In cursus pellentesque elit a tempor. Nullam maximus tincidunt tortor in finibus. Quisque eleifend, augue nec fermentum imperdiet, orci nisi interdum neque, non cursus quam orci eu eros. Aenean id bibendum enim.", "Donec elementum, nibh et lobortis dapibus, ex dui auctor magna, eu tempor justo nibh ac neque. Duis fermentum, arcu vel vestibulum tincidunt, odio libero maximus quam, eget interdum erat sapien vel erat. Etiam id nisi tellus. Sed vel fermentum nulla. Mauris sagittis ornare eros non cursus. Mauris pretium ligula libero, eleifend cursus risus aliquam consequat. Pellentesque vestibulum, augue vel consequat mattis, lacus massa rutrum augue, ut venenatis nunc nulla id enim. Etiam id malesuada nibh, non porta tellus. Vivamus viverra porta tortor. In sit amet molestie arcu, ut sodales magna. Nullam in nibh velit. Phasellus malesuada est nec nisl porta ultrices. Nam vulputate ex blandit cursus vehicula.", "Aenean non sapien mi. Aliquam dui tellus, condimentum non enim convallis, maximus viverra quam. Quisque facilisis nunc turpis, nec accumsan arcu tincidunt ut. Nulla commodo massa eget urna viverra, dapibus sagittis lacus accumsan. Ut accumsan viverra tellus et rhoncus. Ut ut cursus urna. Pellentesque ultrices, neque quis dictum faucibus, quam justo blandit odio, quis placerat ligula nisl ut diam. Nam fringilla velit sapien, vitae rhoncus metus rhoncus eget. Nullam non ante sit amet tortor pharetra viverra. Aliquam non tellus mi. Nulla euismod fermentum ligula eget molestie. Mauris vulputate mollis magna eget egestas. Maecenas imperdiet sit amet dui quis condimentum."]
    },
    {
      id:7,
      visible:true,
      title:"Domino",
      img:"../assets/img/Munchkin.png",
      tags:["luck", "strategy"],
      intro:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      text:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed bibendum lorem. Duis elementum id tortor et commodo. Quisque in arcu maximus, placerat lorem at, tristique justo. Vivamus a diam tristique, scelerisque dui eu, scelerisque mauris. In cursus pellentesque elit a tempor. Nullam maximus tincidunt tortor in finibus. Quisque eleifend, augue nec fermentum imperdiet, orci nisi interdum neque, non cursus quam orci eu eros. Aenean id bibendum enim.", "Donec elementum, nibh et lobortis dapibus, ex dui auctor magna, eu tempor justo nibh ac neque. Duis fermentum, arcu vel vestibulum tincidunt, odio libero maximus quam, eget interdum erat sapien vel erat. Etiam id nisi tellus. Sed vel fermentum nulla. Mauris sagittis ornare eros non cursus. Mauris pretium ligula libero, eleifend cursus risus aliquam consequat. Pellentesque vestibulum, augue vel consequat mattis, lacus massa rutrum augue, ut venenatis nunc nulla id enim. Etiam id malesuada nibh, non porta tellus. Vivamus viverra porta tortor. In sit amet molestie arcu, ut sodales magna. Nullam in nibh velit. Phasellus malesuada est nec nisl porta ultrices. Nam vulputate ex blandit cursus vehicula.", "Aenean non sapien mi. Aliquam dui tellus, condimentum non enim convallis, maximus viverra quam. Quisque facilisis nunc turpis, nec accumsan arcu tincidunt ut. Nulla commodo massa eget urna viverra, dapibus sagittis lacus accumsan. Ut accumsan viverra tellus et rhoncus. Ut ut cursus urna. Pellentesque ultrices, neque quis dictum faucibus, quam justo blandit odio, quis placerat ligula nisl ut diam. Nam fringilla velit sapien, vitae rhoncus metus rhoncus eget. Nullam non ante sit amet tortor pharetra viverra. Aliquam non tellus mi. Nulla euismod fermentum ligula eget molestie. Mauris vulputate mollis magna eget egestas. Maecenas imperdiet sit amet dui quis condimentum."]
    },
    {
      id:8,
      visible:true,
      title:"Manillen",
      img:"../assets/img/Azul.png",
      tags:["luck", "strategy", "cards"],
      intro:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      text:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed bibendum lorem. Duis elementum id tortor et commodo. Quisque in arcu maximus, placerat lorem at, tristique justo. Vivamus a diam tristique, scelerisque dui eu, scelerisque mauris. In cursus pellentesque elit a tempor. Nullam maximus tincidunt tortor in finibus. Quisque eleifend, augue nec fermentum imperdiet, orci nisi interdum neque, non cursus quam orci eu eros. Aenean id bibendum enim.", "Donec elementum, nibh et lobortis dapibus, ex dui auctor magna, eu tempor justo nibh ac neque. Duis fermentum, arcu vel vestibulum tincidunt, odio libero maximus quam, eget interdum erat sapien vel erat. Etiam id nisi tellus. Sed vel fermentum nulla. Mauris sagittis ornare eros non cursus. Mauris pretium ligula libero, eleifend cursus risus aliquam consequat. Pellentesque vestibulum, augue vel consequat mattis, lacus massa rutrum augue, ut venenatis nunc nulla id enim. Etiam id malesuada nibh, non porta tellus. Vivamus viverra porta tortor. In sit amet molestie arcu, ut sodales magna. Nullam in nibh velit. Phasellus malesuada est nec nisl porta ultrices. Nam vulputate ex blandit cursus vehicula.", "Aenean non sapien mi. Aliquam dui tellus, condimentum non enim convallis, maximus viverra quam. Quisque facilisis nunc turpis, nec accumsan arcu tincidunt ut. Nulla commodo massa eget urna viverra, dapibus sagittis lacus accumsan. Ut accumsan viverra tellus et rhoncus. Ut ut cursus urna. Pellentesque ultrices, neque quis dictum faucibus, quam justo blandit odio, quis placerat ligula nisl ut diam. Nam fringilla velit sapien, vitae rhoncus metus rhoncus eget. Nullam non ante sit amet tortor pharetra viverra. Aliquam non tellus mi. Nulla euismod fermentum ligula eget molestie. Mauris vulputate mollis magna eget egestas. Maecenas imperdiet sit amet dui quis condimentum."]
    },
    {
      id:9,
      visible:true,
      title:"Poker",
      img:"../assets/img/Munchkin.png",
      tags:["luck", "strategy", "cards"],
      intro:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      text:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed bibendum lorem. Duis elementum id tortor et commodo. Quisque in arcu maximus, placerat lorem at, tristique justo. Vivamus a diam tristique, scelerisque dui eu, scelerisque mauris. In cursus pellentesque elit a tempor. Nullam maximus tincidunt tortor in finibus. Quisque eleifend, augue nec fermentum imperdiet, orci nisi interdum neque, non cursus quam orci eu eros. Aenean id bibendum enim.", "Donec elementum, nibh et lobortis dapibus, ex dui auctor magna, eu tempor justo nibh ac neque. Duis fermentum, arcu vel vestibulum tincidunt, odio libero maximus quam, eget interdum erat sapien vel erat. Etiam id nisi tellus. Sed vel fermentum nulla. Mauris sagittis ornare eros non cursus. Mauris pretium ligula libero, eleifend cursus risus aliquam consequat. Pellentesque vestibulum, augue vel consequat mattis, lacus massa rutrum augue, ut venenatis nunc nulla id enim. Etiam id malesuada nibh, non porta tellus. Vivamus viverra porta tortor. In sit amet molestie arcu, ut sodales magna. Nullam in nibh velit. Phasellus malesuada est nec nisl porta ultrices. Nam vulputate ex blandit cursus vehicula.", "Aenean non sapien mi. Aliquam dui tellus, condimentum non enim convallis, maximus viverra quam. Quisque facilisis nunc turpis, nec accumsan arcu tincidunt ut. Nulla commodo massa eget urna viverra, dapibus sagittis lacus accumsan. Ut accumsan viverra tellus et rhoncus. Ut ut cursus urna. Pellentesque ultrices, neque quis dictum faucibus, quam justo blandit odio, quis placerat ligula nisl ut diam. Nam fringilla velit sapien, vitae rhoncus metus rhoncus eget. Nullam non ante sit amet tortor pharetra viverra. Aliquam non tellus mi. Nulla euismod fermentum ligula eget molestie. Mauris vulputate mollis magna eget egestas. Maecenas imperdiet sit amet dui quis condimentum."]
    },
    {
      id:10,
      visible:true,
      title:"Scrabble",
      img:"../assets/img/Azul.png",
      tags:["luck", "strategy", "board"],
      intro:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      text:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed bibendum lorem. Duis elementum id tortor et commodo. Quisque in arcu maximus, placerat lorem at, tristique justo. Vivamus a diam tristique, scelerisque dui eu, scelerisque mauris. In cursus pellentesque elit a tempor. Nullam maximus tincidunt tortor in finibus. Quisque eleifend, augue nec fermentum imperdiet, orci nisi interdum neque, non cursus quam orci eu eros. Aenean id bibendum enim.", "Donec elementum, nibh et lobortis dapibus, ex dui auctor magna, eu tempor justo nibh ac neque. Duis fermentum, arcu vel vestibulum tincidunt, odio libero maximus quam, eget interdum erat sapien vel erat. Etiam id nisi tellus. Sed vel fermentum nulla. Mauris sagittis ornare eros non cursus. Mauris pretium ligula libero, eleifend cursus risus aliquam consequat. Pellentesque vestibulum, augue vel consequat mattis, lacus massa rutrum augue, ut venenatis nunc nulla id enim. Etiam id malesuada nibh, non porta tellus. Vivamus viverra porta tortor. In sit amet molestie arcu, ut sodales magna. Nullam in nibh velit. Phasellus malesuada est nec nisl porta ultrices. Nam vulputate ex blandit cursus vehicula.", "Aenean non sapien mi. Aliquam dui tellus, condimentum non enim convallis, maximus viverra quam. Quisque facilisis nunc turpis, nec accumsan arcu tincidunt ut. Nulla commodo massa eget urna viverra, dapibus sagittis lacus accumsan. Ut accumsan viverra tellus et rhoncus. Ut ut cursus urna. Pellentesque ultrices, neque quis dictum faucibus, quam justo blandit odio, quis placerat ligula nisl ut diam. Nam fringilla velit sapien, vitae rhoncus metus rhoncus eget. Nullam non ante sit amet tortor pharetra viverra. Aliquam non tellus mi. Nulla euismod fermentum ligula eget molestie. Mauris vulputate mollis magna eget egestas. Maecenas imperdiet sit amet dui quis condimentum."]
    },
    {
      id:11,
      visible:true,
      title:"Pokemon Red/Blue",
      img:"../assets/img/Munchkin.png",
      tags:["luck", "strategy", "video"],
      intro:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      text:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed bibendum lorem. Duis elementum id tortor et commodo. Quisque in arcu maximus, placerat lorem at, tristique justo. Vivamus a diam tristique, scelerisque dui eu, scelerisque mauris. In cursus pellentesque elit a tempor. Nullam maximus tincidunt tortor in finibus. Quisque eleifend, augue nec fermentum imperdiet, orci nisi interdum neque, non cursus quam orci eu eros. Aenean id bibendum enim.", "Donec elementum, nibh et lobortis dapibus, ex dui auctor magna, eu tempor justo nibh ac neque. Duis fermentum, arcu vel vestibulum tincidunt, odio libero maximus quam, eget interdum erat sapien vel erat. Etiam id nisi tellus. Sed vel fermentum nulla. Mauris sagittis ornare eros non cursus. Mauris pretium ligula libero, eleifend cursus risus aliquam consequat. Pellentesque vestibulum, augue vel consequat mattis, lacus massa rutrum augue, ut venenatis nunc nulla id enim. Etiam id malesuada nibh, non porta tellus. Vivamus viverra porta tortor. In sit amet molestie arcu, ut sodales magna. Nullam in nibh velit. Phasellus malesuada est nec nisl porta ultrices. Nam vulputate ex blandit cursus vehicula.", "Aenean non sapien mi. Aliquam dui tellus, condimentum non enim convallis, maximus viverra quam. Quisque facilisis nunc turpis, nec accumsan arcu tincidunt ut. Nulla commodo massa eget urna viverra, dapibus sagittis lacus accumsan. Ut accumsan viverra tellus et rhoncus. Ut ut cursus urna. Pellentesque ultrices, neque quis dictum faucibus, quam justo blandit odio, quis placerat ligula nisl ut diam. Nam fringilla velit sapien, vitae rhoncus metus rhoncus eget. Nullam non ante sit amet tortor pharetra viverra. Aliquam non tellus mi. Nulla euismod fermentum ligula eget molestie. Mauris vulputate mollis magna eget egestas. Maecenas imperdiet sit amet dui quis condimentum."]
    },
    {
      id:12,
      visible:true,
      title:"Super Mario Bros",
      img:"../assets/img/Azul.png",
      tags:["video"],
      intro:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      text:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed bibendum lorem. Duis elementum id tortor et commodo. Quisque in arcu maximus, placerat lorem at, tristique justo. Vivamus a diam tristique, scelerisque dui eu, scelerisque mauris. In cursus pellentesque elit a tempor. Nullam maximus tincidunt tortor in finibus. Quisque eleifend, augue nec fermentum imperdiet, orci nisi interdum neque, non cursus quam orci eu eros. Aenean id bibendum enim.", "Donec elementum, nibh et lobortis dapibus, ex dui auctor magna, eu tempor justo nibh ac neque. Duis fermentum, arcu vel vestibulum tincidunt, odio libero maximus quam, eget interdum erat sapien vel erat. Etiam id nisi tellus. Sed vel fermentum nulla. Mauris sagittis ornare eros non cursus. Mauris pretium ligula libero, eleifend cursus risus aliquam consequat. Pellentesque vestibulum, augue vel consequat mattis, lacus massa rutrum augue, ut venenatis nunc nulla id enim. Etiam id malesuada nibh, non porta tellus. Vivamus viverra porta tortor. In sit amet molestie arcu, ut sodales magna. Nullam in nibh velit. Phasellus malesuada est nec nisl porta ultrices. Nam vulputate ex blandit cursus vehicula.", "Aenean non sapien mi. Aliquam dui tellus, condimentum non enim convallis, maximus viverra quam. Quisque facilisis nunc turpis, nec accumsan arcu tincidunt ut. Nulla commodo massa eget urna viverra, dapibus sagittis lacus accumsan. Ut accumsan viverra tellus et rhoncus. Ut ut cursus urna. Pellentesque ultrices, neque quis dictum faucibus, quam justo blandit odio, quis placerat ligula nisl ut diam. Nam fringilla velit sapien, vitae rhoncus metus rhoncus eget. Nullam non ante sit amet tortor pharetra viverra. Aliquam non tellus mi. Nulla euismod fermentum ligula eget molestie. Mauris vulputate mollis magna eget egestas. Maecenas imperdiet sit amet dui quis condimentum."]
    },
    {
      id:13,
      visible:true,
      title:"Tetris",
      img:"../assets/img/Munchkin.png",
      tags:["luck", "strategy", "video"],
      intro:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      text:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed bibendum lorem. Duis elementum id tortor et commodo. Quisque in arcu maximus, placerat lorem at, tristique justo. Vivamus a diam tristique, scelerisque dui eu, scelerisque mauris. In cursus pellentesque elit a tempor. Nullam maximus tincidunt tortor in finibus. Quisque eleifend, augue nec fermentum imperdiet, orci nisi interdum neque, non cursus quam orci eu eros. Aenean id bibendum enim.", "Donec elementum, nibh et lobortis dapibus, ex dui auctor magna, eu tempor justo nibh ac neque. Duis fermentum, arcu vel vestibulum tincidunt, odio libero maximus quam, eget interdum erat sapien vel erat. Etiam id nisi tellus. Sed vel fermentum nulla. Mauris sagittis ornare eros non cursus. Mauris pretium ligula libero, eleifend cursus risus aliquam consequat. Pellentesque vestibulum, augue vel consequat mattis, lacus massa rutrum augue, ut venenatis nunc nulla id enim. Etiam id malesuada nibh, non porta tellus. Vivamus viverra porta tortor. In sit amet molestie arcu, ut sodales magna. Nullam in nibh velit. Phasellus malesuada est nec nisl porta ultrices. Nam vulputate ex blandit cursus vehicula.", "Aenean non sapien mi. Aliquam dui tellus, condimentum non enim convallis, maximus viverra quam. Quisque facilisis nunc turpis, nec accumsan arcu tincidunt ut. Nulla commodo massa eget urna viverra, dapibus sagittis lacus accumsan. Ut accumsan viverra tellus et rhoncus. Ut ut cursus urna. Pellentesque ultrices, neque quis dictum faucibus, quam justo blandit odio, quis placerat ligula nisl ut diam. Nam fringilla velit sapien, vitae rhoncus metus rhoncus eget. Nullam non ante sit amet tortor pharetra viverra. Aliquam non tellus mi. Nulla euismod fermentum ligula eget molestie. Mauris vulputate mollis magna eget egestas. Maecenas imperdiet sit amet dui quis condimentum."]
    },
    {
      id:14,
      visible:true,
      title:"Captain Sonar",
      img:"../assets/img/CaptSonar.png",
      tags:["strategy", "board"],
      intro:"Das Boot, de boardgame. In Captain Sonar nemen 2 spelersgroepen in hun eigen duikboot het tegen elkaar op, elke speler heeft een unieke rol en beïnvloedt de dynamiek van het spel. Samenwerking en kennis van je ploeggenoten hun zwaktes/sterktes zijn essentieel. Van 2-8 spelers, waarbij het eigenlijk pas leuk wordt vanaf 6! spelers. Vanaf 12 jaar, Spelduur: 45 minuten, niet incl downtime. Richtprijs: 48,99 euro. Designer: Roberto Fraga, Yohan Lemonnier. Uitgever: Matagot.  ",
      text:["In Captain Sonar speel je in twee teams tegen elkaar. Elk team zit samen in een onderzeeër met maar één doel: de tegenstander laten zinken. Bovendien heeft iedere speler een eigen unieke rol binnen het team: Kapitein, First Mate, Engineer of Radio Operator.","Als je dit spel speelt, vliegt de tijd voorbij. Voordat je het weet ben je de halve kaart afgereisd en ben je al twintig minuten verder. Je hebt geen enkel dood spelmoment, want iedereen is constant tegelijkertijd hun eigen rol aan het uitvoeren. Naarmate je steeds beter weet waar de tegenstander is, wordt iedereen steeds enthousiaster. Het is ook echt heel spannend. Je hoort het andere team smiespelen en denkt: weten ze waar we zijn? Of zitten ze ernaast? Of is dit een bluf? Dan zegt je Radio Operator ineens: “volgens mij weet ik waar ze zijn”. We bewegen subtiel dichterbij, laden onze torpedo op, en vuren hem af. We houden onze adem in. Hebben we hen geraakt of niet? Dan komt het verlossende antwoord: “indirecte hit”. We weten dat ze in de buurt zijn, maar waar precies? Sterker nog: om de torpedo af te vuren moeten wij ook in de buurt van de anderen zijn, dus nu kunnen zij ook op ons schieten!Richting het einde van het eerste potje was de kapitein van het andere team haast aan het gillen van enthousiasme. n is het een goed spel :pIk heb het daarna ook nog met een andere groep gespeeld, en die wilden meteen een tweede potje, en de volgende dag nóg een potje.","De onderdelen van het spel zijn van goede kwaliteit. De stiften schrijven goed en zijn ook goed uit te gummen. De vellen zijn stevig, ook de doorschijnende platen voor de Radio Operator. De doos is zeer stevig (ik krijg hem nauwelijks open of dicht). Het spelersscherm dat tussen beide teams zit is enorm groot en stevig! (Iemand liep een keertje weg vlak voordat ik het spel begon op te zetten, en toen hij drie minuten later terug kwam schrok hij van wat er ineens op tafel was verschenen.)","Als je een keer een grote groep hebt, zeker als daar verschillende leeftijden en soorten spelers inzitten, is dit spel een aanrader! Zorg dat je de regels goed doorleest en een “beginnersvariant” bedenkt om mensen op een leuke en geleidelijke manier alle rollen en gadgets in het spel uit te leggen. Als je een groep hebt met – tja – stomme spelers, is dit spel geen goed idee. Het is zeer competitief en de kans op valsspelen is groot. (Met “stomme spelers” bedoel ik niet stomme mensen, maar mensen die niet goed weten hoe ze een bordspel moeten spelen. Mensen die bijvoorbeeld niet tegen hun verlies kunnen, of altijd zeuren, of de boel ophouden, of niet luisteren als je ze corrigeert, etc.)",""]
    },
    {
      id:15,
      visible:true,
      title:"Azul",
      img:"../assets/img/Azul.png",
      tags:["strategy", "board"],
      intro:"Demonstreer je talenten als Portugese azulejos tegellegger. Azul is een tile-placement spel waarbij de spelers via strategische keuzes en wonderbaarlijk simpele regels een onderlinge competitie voeren met leuke dynamiek. Winnaar van veel verschillende prijzen. Vanaf 8 jaar, geschikt van 2 tot 4 spelers. Spelduur: 40 minuten, incl downtime. Richtprijs: 38.99 euro. Designer: Michael Kiesling. Uitgever: Next Move Games.  ",
      text:["Azuleojos (oorspronkelijk witte en blauwe keramische tegels) zijn een uitvinding van de Moren. De versiering valt in de smaak bij de Portugese koning Manuel I tijdens zijn bezoek aan het Alhambra, in het zuiden van Spanje. Hij is zo onder de indruk van de fabelachtige schoonheid van de Moorse decoraties in het interieur van het Alhambra dat hij zijn eigen paleis in Portugal met soortgelijke tegels laat versieren. Het is aan jullie om in opdracht van de koning de muren van het koninklijke paleis in Evora te verfraaien.","Azul is helemaal in de stijl van de kenmerkende Portugese tegels. Het spelmateriaal is mooi versierd en de vijf verschillende soorten tegels zijn kleurrijk en geïnspireerd op echte motieven. Ondanks dat de tegels van plastic zijn, lijken ze best wel op geglazuurde keramieken tegels. Het thema van de Portugese tegels past prima bij het spel en zorgt er gelijk voor dat het spel er aantrekkelijk uitziet, het oog wil immers ook wat.Het spel bestaat uit 100 tegels en in totaal 19 andere spelmaterialen. Het opzetten gaat verrassend snel. De tegels gaan in het meegeleverde zakje, iedere speler krijgt een scoretableau en steentje om de score bij te houden en afhankelijk van het aantal spelers leg je in het midden een aantal fabrieksschijven neer. Binnen 5 minuten stond bij mij Azul klaar om te spelen.","In je beurt verzamel je tegels, deze pak je óf van de fabrieksschijven, óf vanuit de aflegpot in het midden van de tafel. Op iedere fabrieksschijf liggen vier tegels. Pak je in je beurt van een schijf, dan pak je daarvan alle tegels van dezelfde kleur. (Dit kunnen er maximaal 4 en minimaal 1 zijn) De overige tegels (in andere kleuren) leg je in de aflegpot. Op deze manier komen er steeds meer tegels in de aflegpot terecht. Je kunt er ook voor kiezen om alle tegels van dezelfde kleur uit de aflegpot te pakken. Er worden beurtelings net zolang tegels gepakt totdat alle tegels op zijn.De tegels die je gepakt hebt leg je op je tableau neer. Er zijn in totaal vijf rijen om tegels neer te leggen, in de eerste rij is er plek voor één tegel, de tweede rij twee tegels, enzovoort. Er mogen alleen dezelfde kleur tegels bij elkaar in een rij liggen. Voor tegels die je niet kwijt kunt op je tableau krijg je minpunten. Zijn alle tegels gepakt, dan controleert iedere speler welke tegels van zijn tableau naar zijn muur kunnen. Is er een rij volledig gevuld met tegels dan gaat er één tegel uit een rij naar de muur op de daarvoor bestemde plek. Alle andere tegels worden afgelegd. Voor elke tegel die je op je muur plaatst krijg je een punt, liggen er al andere tegels in dezelfde rij of kolom dan krijg je extra punten. Alle tegels in een rij die niet volledig gevuld is blijven liggen. Nu begint de volgende ronde en worden alle fabrieksschijven weer aangevuld. Zijn de tegels op, dan wordt de zak weer gevuld met alle afgelegde tegels. De spelregels van Azul zijn niet moeilijk en passen zo’n drie A4’tjes. Na 15 minuten lezen had ik door hoe het spel gespeeld wordt. Snap je eenmaal de regels dan is het spel ook zo uit te leggen aan nieuwe spelers.","In de basis is Azul een abstract spel waarbij je vijf verschillende kleuren in een bepaald patroon moet zien te krijgen. Wil je de meeste punten scoren, dan probeer je zoveel mogelijk tegels aaneengesloten op je muur plaatsen. Natuurlijk lukt dit niet altijd omdat je wordt gedwongen bepaalde tegels te pakken die niet binnen je strategie passen. Soms kan het daarom erg waardevol zijn om bepaalde tegels te laten liggen voor je tegenstander zodat hij of zij (veel)minpunten scoort. Het abstracte spelen is niet voor iedereen weggelegd. Sommige mensen hebben moeite met ruimtelijk inzicht of vinden er gewoon niets aan. Het moet wel een beetje je ding zijn om tegels in een bepaalde kleur te willen verzamelen en deze vervolgens in een zo’n efficiënt mogelijk manier te plaatsen. Zelf vind ik het wel leuk om op die manier een beetje te puzzelen, maar dit geldt niet voor iedereen. Ondanks dat geen spel precies hetzelfde is doordat de tegels willekeurig neergelegd worden, vind ik het spel op een gegeven moment wel beetje eentonig worden. Heb je Azul een aantal keren gespeeld dan heb je snel door wat de beste strategie is. Hierdoor verval je elk spel weer in hetzelfde trucje en hoop je vooral dat je geluk hebt dat de tegels voor jou net iets gunstiger uitpakken dan voor je medespeler(s). Dit betekent niet dat je met dezelfde strategie altijd gegarandeerd wint, maar dat er weinig variatie zit in de manieren waarop je het spel kunt winnen. Wil je het spel nog iets uitdagender maken dan zit er gelukkig wel een moeilijkere spelvariant in de spelregels.","Azul is een tactisch spel, je moet goed vooruit plannen en nadenken welke tegels je op welk moment pakt. Je wilt namelijk voorkomen dat je aan het einde van een ronde opgescheept raakt met tegels die je niet meer kwijt kunt op je tableau omdat je aan het begin van je beurt de tegels niet slim hebt neergelegd. Ook moet je goed puzzelen zodat je, bij het plaatsen van tegels op je muur, de meeste punten scoort. Geluk speelt ook een factor, maar wel een stuk minder. Je kunt geluk hebben dat er precies het voldoende aantal tegels in een specifieke kleur voor het oprapen liggen. De interactie met je medespelers is gemiddeld, je speelt vooral je eigen spel, maar een goede speler let ook op of hij ongunstige tegels kan laten liggen voor de andere speler(s). e speelt Azul met 2 tot 4 spelers. Hoe meer spelers er mee doen hoe uitdagender het spel wordt. Dit omdat er dan meer concurrentie is om de hoogste score en ook omdat het lastiger wordt om alle tegenstanders dwars te zitten, meestal kun je alleen de speler na jou hinderen. Azul is ook erg goed met twee spelers te spelen. Speel je met z’n tweeën dan wordt het nog belangrijker om goed te kijken hoe je je tegenstander kan laten zitten met ongunstige tegels voor de minste punten. Een potje duurt ongeveer 40 minuten al kan dit ook in de helft van de tijd wanneer je met z’n tweeën en bekend met het spel bent. Azul is geschikt vanaf 8 jaar al heb ik vernomen dat voor de meeste jonge kinderen het spel nog iets te abstract is om goed te kunnen snappen."]
    },
    {
      id:16,
    visible:true,
    title:"Munchkin",
    img:"../assets/img/Munchkin.png",
    tags:["luck", "strategy", "cards",],
    intro:"Munchkin is een stand alone card-game/deckbuilder met heel veel player-player interactie die zich afspeelt in een hilarisch Dark-Fantasy universum. Geschikt van 3-8 spelers, vanaf 10jaar met een gemiddelde speltijd van ongeveer 60 minuten, downtime inbegrepen. Winnaar van prijzen. Lichte game, Ideaal voor met familie of vrienden als er geen tijd is om diep in een rulebook te duiken of als er een ergens eens een gaatje van een uur te vullen is. Richtprijs: 27,95 euro. Designer: Steve Jackson; Uitgever: Steve Jackson Games.",
    text:["Als je met een groepje vrienden op verkenning wilt gaan in kerkers, tegen monsters wilt vechten en schatten wilt verzamelen, dan kun je een potje co-op spelen. Heb je echter even geen zin om te gamen, dan is er Munchkin. Maar wat is een Munchkin eigenlijk? In de wereld van bordspellen is een munchkin iemand die veel te hard zijn best doet om te winnen tijdens een vriendschappelijk spelletje. Dus bijvoorbeeld iemand die een zonnebril opzet tijdens een vriendschappelijk potje poker. In Munchkin is het dan ook de bedoeling om te winnen, op welke manier dan ook. Zelfs als dit betekent dat je je beste vriend een mes in de rug moet steken.","Munchkin is een role-playing game in kaartspelvorm. Elke speler start als een normaal mens zonder bescherming of wapens. Gaandeweg zul je sterker worden en krijg je allerlei wapens om monsters mee te verslaan. Het spel is speelbaar met drie tot zes personen. Voor deze recensie hebben we het spel getest met vijf mensen. Het werd al wel snel duidelijk dat de voorgeschreven spelduur van 60 minuten, sterk onderschat is. Het eerste potje duurde zo'n twee uur en ook de volgende potjes duurden heel wat langer dan één uur. Doel van het spel? Als eerste level tien bereiken. Je stijgt in level door zeer sterke monsters te verslaan of door een 'ga een level omhoog'-kaart te spelen. Het winnende level kun je enkel scoren door een monster te verslaan.","Monsters verslaan is niet eenvoudig in het begin, wanneer je aanvalslevel zwak is. Gelukkig kun je te allen tijde je medespelers om hulp vragen. Je stelt een beloning voor en vervolgens mag je de hulp accepteren van iedereen die wil helpen. Wanneer je een laag level hebt, helpt iedereen je met plezier. Maar wanneer je, door sterke monsters te verslaan en bepaalde kaarten te trekken, al wat dichter bij level tien komt, keert iedereen zich tegen je. In Munchkin keren de zwakkere leden zich steeds tegen de sterkeren die bijna de overwinning binnen hebben."
    ,"Hier begint het beste stuk. Het is namelijk de bedoeling dat je de gemeenste kaarten achter de hand houdt tot je iemand erg hard kunt tegenwerken. Bijvoorbeeld: Iemand is maar net sterk genoeg om een monster te verslaan. Om hem tegen te werken kun je het monster met bepaalde kaarten in level laten stijgen. Dit kan zware gevolgen hebben. Je verloren medespeler kan hierdoor levels verliezen of zelfs sterven voor een volle ronde. Wanneer je sterft mag elke speler een van je kaarten stelen. Het hele spel lang zit je in je hoofd plannetjes te ontwerpen om mensen op de gemeenste manieren een mes in de rug te steken. Je kunt dus niemand vertrouwen, terwijl je in sommige situaties geen keuze hebt en wel hulp moet aanvaarden. Zo speelt Munchkin rare spelletjes in je hoofd, waardoor je helemaal paranoïde kunt worden. Het is hilarisch om te zien hoe iedereen elkaar probeert te pesten.","Het sterkste punt van Munchkin is ongetwijfeld de mogelijkheid om je vrienden te pesten, maar er is nog een ander belangrijk hoogtepunt. De kaarten zelf! Deze zijn geweldig vormgegeven en vormen een parodie op andere bordspellen, videogames, televisiereeksen en zelfs het internet. Elke kaart heeft een titel en een afbeelding die haast perfect op elkaar zijn ingestemd. Zo zul je bijvoorbeeld moeten vechten tegen de gevreesde schaamluis. Een monster waar je overigens niet van kunt wegrennen. Andere kaarten als kontschoppende laarzen, de eend des onheils en het kamerplantje kunnen ook steeds weer op heel wat gelach rekenen. Haast elke kaart brengt wel een glimlach op je gezicht. De combinatie tussen de grappige kaarten en het klooien van je vrienden kan dan ook echt hilarische momenten opleveren.","Munchkin is een erg origineel en leuk kaartspel dat zichzelf niet te serieus neemt. Maar goed ook, want de charme van dit spel zit in de humor van de kaarten. Helaas komt er een einde aan deze charme wanneer je na een vijftal potjes alle kaarten wel uit je hoofd kent. Om de humor erin te houden, kun je het spel dus het beste niet te snel na elkaar spelen. Het is geweldig om te zien hoe iedereen zijn best doet om andere spelers op de gemeenst mogelijke manieren tegen te werken. Het einde had wel wat spannender gemogen, maar de lange reis naar het einde toe is steeds uniek en verdomd amusant. Een ideaal spel voor iedereen die op zoek is naar een luchtig kaartspel en goede humor kan waarderen."]
    },
    {
      id:17,
      visible:true,
      title:"Schaken",
      img:"../assets/img/schaken.jpg",
      tags:["board", "strategy"],
      intro:"Kokkie neemt voor het eerst een kijkje naar het eeuwenoude klassieke schaakspel. Hij deelt zijn inzichten inzake aanvallen, verdedigen en het afmaken van je tegenstander, maar is hij opgewassen tegen ervaren grootmeesters?",
      text:["Ah schaken. Berucht en beroemd. Toch schrikt het vele mensen af met zijn reputatie voor zwaar denkwerk en analyse. Vandaag kom ik, Kokkie, jullie vertellen wat ik vind van de grootvader van bordspellen",
      "De look van een schaakspel is natuurlijk iconisch: duidelijk contrast tussen kleuren en herkenbare speelstukken in zowat alle versies die er bestaan. Het is niet te druk, en zelf als je niet kan spelen kan het intelligent overkomen om een schaakspel in je living te hebben. Toch moet ik zeggen dat ik enerzijds wel wat visuele complexiteit mis, maar anderzijds kan ik me dan volledig concentreren op het denkwerk.",
      "Het spelverloop is vrij simpel: de speler met de witte stukken beweegt een stuk, de speler met de zwarte stukken doet hetzelfde. Repeat. De uitdaging is natuurlijk het juiste stuk naar het juiste veld bewegen. Deze juiste zet is afhankelijk van je plan. Dit plan kan zijn om een stuk te winnen, een aanval te blokkeren of zelfs simpelweg te winnen. Zo wordt schaken een strategisch steekspel, waarbij je je tegenspeler moet overtreffen met je tactieken.",
      "Ik heb wat potjes gespeeld tegen een kennis van me, Hikaru Nakamura. Hij kon er duidelijk wel wat van, en na een lange uitputtingsslag moest ik in hem mijn meerdere erkennen. Hij had natuurlijk wel al wat ervaring, maar ik denk dat ik hem toch goed heb laten zweten. Zo'n meeting of the minds heeft toch iets magisch, en hoewel we quasi de hele partij stil waren, was er toch een soort verhaal dat zich afspeelde, enkel door wat figuurtjes te bewegen op tegels.",
      "Ik heb zeker genoten van deze smaakmaker van het schaakspel. Het is duidelijk een spel met zeer diepgaande strategieën, en ik zou mij er gemakkelijk maanden in kunnen verdiepen. Maar als je echt goed wil zijn, moet je bereid zijn om er veel tijd in te steken, en dan moet je toch wel een fan zijn van al dat denken. Het is dus zeker de moeite om het eens te bekijken, maar studeer er niet voor tenzij het echt je ding is."]
      },
      {
        id:18,
        visible:true,
        title:"Dungeons and Dragons",
        img:"../assets/img/dnd.png",
        tags:["luck", "strategy", "cards",],
        intro:"Peppie en Kokkie gaan op avontuur in de Forgotten Realms, op zoek naar, jawel, draken en kerkers. En misschien ook wat schatten. Lees hier het verslag van hun eerste echte roleplaying ervaring.",
        text:["Peppie de vechter en Kokkie de tovenaar kijken vanop de heuvel waar ze staan neer op het dorpje Greenest. Plots vliegen verschillende gebouwen in lichterlaaie en horen ze schreeuwen van paniek. Peppie neemt zijn bijl in de hand en snelt de heuvel af. Kokkie haalt zijn schouders op, slaakt een zucht en loopt zijn kompaan achterna. Zo begint het avontuur van Peppie en Kokkie.",
        "D&D is een 'roleplaying game'. Dit wil zeggen dat je echt een personage bent, en je als dat personage in de wereld keuzes maakt. Dit gaat van kleine dingen zoals 'Geef ik de barman een fooi?' tot monumentale beslissingen zoals 'Welk koninkrijk steun ik in deze oorlog?'. Deze keuzes sturen het verhaal dat zich afspeelt, waardoor de avonturen eindeloos veel mogelijkheden hebben.",
        "Maar al deze opties worden bestuurd door 1 van de spelers, genaamd de Dungeon Master (DM) of Game Master (GM). Deze speler heeft geen eigen personage, maar bestuurt de wereld. Hij is dus zowel de arme boer die de spelers om hulp smeekts als de machtige draak die met gemak een dorp kan platbranden. D&D is dus een asymmetrisch spel, waarbij een speler de context bepaald waarin de anderen kunnen 'roleplayen'.",
        "Elke speler behalve de DM heeft een blad met allerlei attributen en getallen. Dit blad bepaalt wat een speler heeft, kan en waar hij/zij goed of slecht in is. Deze attributen, in combinatie met dobbelstenen, bepalen of de speler een bepaalde actie succesvol uitvoert. De draak mag dan wel een harde huid hebben waar Peppie's bijl niet veel tegen kan, maar hij is misschien niet snel genoeg om Kokkie's bliksem te ontwijken.",
        "Dit samenspel van personages en de wereld (via de DM), tesamen met de quasi eindeloze opties maken van D&D een unieke ervaring die je niet gemakkelijk zal vinden in een traditioneel bordspel. Het grote nadeel is dat het veel tijd vraagt, zowel ter voorbereiding als om te spelen. Ook kan de combinatie van boeken en accessoires was prijzig uitvallen, maar eenmaal je ermee weg bent heb je uren aan spelplezier."]
        }
  ];


  visiblePosts =[{
    id:16,
    visible:true,
    title:"Munchkin",
    img:"../assets/img/Munchkin.jpg",
    tags:["luck", "strategy", "cards",],
    intro:"Munchkin is een stand alone card-game/deckbuilder die zich afspeelt in een hilarisch Dark-Fantasy universum. ",
    text:["Als je met een groepje vrienden op verkenning wilt gaan in kerkers, tegen monsters wilt vechten en schatten wilt verzamelen, dan kun je een potje co-op spelen. Heb je echter even geen zin om te gamen, dan is er Munchkin. Maar wat is een Munchkin eigenlijk? In de wereld van bordspellen is een munchkin iemand die veel te hard zijn best doet om te winnen tijdens een vriendschappelijk spelletje. Dus bijvoorbeeld iemand die een zonnebril opzet tijdens een vriendschappelijk potje poker. In Munchkin is het dan ook de bedoeling om te winnen, op welke manier dan ook. Zelfs als dit betekent dat je je beste vriend een mes in de rug moet steken.","Munchkin is een role-playing game in kaartspelvorm. Elke speler start als een normaal mens zonder bescherming of wapens. Gaandeweg zul je sterker worden en krijg je allerlei wapens om monsters mee te verslaan. Het spel is speelbaar met drie tot zes personen. Voor deze recensie hebben we het spel getest met vijf mensen. Het werd al wel snel duidelijk dat de voorgeschreven spelduur van 60 minuten, sterk onderschat is. Het eerste potje duurde zo'n twee uur en ook de volgende potjes duurden heel wat langer dan één uur. Doel van het spel? Als eerste level tien bereiken. Je stijgt in level door zeer sterke monsters te verslaan of door een 'ga een level omhoog'-kaart te spelen. Het winnende level kun je enkel scoren door een monster te verslaan.","Monsters verslaan is niet eenvoudig in het begin, wanneer je aanvalslevel zwak is. Gelukkig kun je te allen tijde je medespelers om hulp vragen. Je stelt een beloning voor en vervolgens mag je de hulp accepteren van iedereen die wil helpen. Wanneer je een laag level hebt, helpt iedereen je met plezier. Maar wanneer je, door sterke monsters te verslaan en bepaalde kaarten te trekken, al wat dichter bij level tien komt, keert iedereen zich tegen je. In Munchkin keren de zwakkere leden zich steeds tegen de sterkeren die bijna de overwinning binnen hebben."
    ,"Hier begint het beste stuk. Het is namelijk de bedoeling dat je de gemeenste kaarten achter de hand houdt tot je iemand erg hard kunt tegenwerken. Bijvoorbeeld: Iemand is maar net sterk genoeg om een monster te verslaan. Om hem tegen te werken kun je het monster met bepaalde kaarten in level laten stijgen. Dit kan zware gevolgen hebben. Je verloren medespeler kan hierdoor levels verliezen of zelfs sterven voor een volle ronde. Wanneer je sterft mag elke speler een van je kaarten stelen. Het hele spel lang zit je in je hoofd plannetjes te ontwerpen om mensen op de gemeenste manieren een mes in de rug te steken. Je kunt dus niemand vertrouwen, terwijl je in sommige situaties geen keuze hebt en wel hulp moet aanvaarden. Zo speelt Munchkin rare spelletjes in je hoofd, waardoor je helemaal paranoïde kunt worden. Het is hilarisch om te zien hoe iedereen elkaar probeert te pesten.","Het sterkste punt van Munchkin is ongetwijfeld de mogelijkheid om je vrienden te pesten, maar er is nog een ander belangrijk hoogtepunt. De kaarten zelf! Deze zijn geweldig vormgegeven en vormen een parodie op andere bordspellen, videogames, televisiereeksen en zelfs het internet. Elke kaart heeft een titel en een afbeelding die haast perfect op elkaar zijn ingestemd. Zo zul je bijvoorbeeld moeten vechten tegen de gevreesde schaamluis. Een monster waar je overigens niet van kunt wegrennen. Andere kaarten als kontschoppende laarzen, de eend des onheils en het kamerplantje kunnen ook steeds weer op heel wat gelach rekenen. Haast elke kaart brengt wel een glimlach op je gezicht. De combinatie tussen de grappige kaarten en het klooien van je vrienden kan dan ook echt hilarische momenten opleveren.","Munchkin is een erg origineel en leuk kaartspel dat zichzelf niet te serieus neemt. Maar goed ook, want de charme van dit spel zit in de humor van de kaarten. Helaas komt er een einde aan deze charme wanneer je na een vijftal potjes alle kaarten wel uit je hoofd kent. Om de humor erin te houden, kun je het spel dus het beste niet te snel na elkaar spelen. Het is geweldig om te zien hoe iedereen zijn best doet om andere spelers op de gemeenst mogelijke manieren tegen te werken. Het einde had wel wat spannender gemogen, maar de lange reis naar het einde toe is steeds uniek en verdomd amusant. Een ideaal spel voor iedereen die op zoek is naar een luchtig kaartspel en goede humor kan waarderen."]
  }]
  constructor() {
    this.blogposts.forEach(post => {
      this.visiblePosts.push(post);
    })
    
   }

  updateVisiblePosts(){
    this.visiblePosts.length = 0;
    this.blogposts.forEach(post => {
      if (post.visible){
      this.visiblePosts.push(post);
      }
    })
  }

  loadBlogPosts(newPosts:number, currentPosts:number){
    let loadedPosts= [];
    if(newPosts+currentPosts <= this.visiblePosts.length)
    {
      let counter = currentPosts;
      while (loadedPosts.length < newPosts && counter <= this.visiblePosts.length) {
          loadedPosts.push(this.visiblePosts[counter]);
        counter++;
      }
    }
    return loadedPosts;
  }

  loadSpecificPost(nr:number){
    return this.blogposts.find(x => x.id == nr);
  }

  sortOldestFirst(){
    this.blogposts = this.blogposts.sort((n1, n2) => {
      if(n1.id > n2.id){
        return 1;
      }
      if(n1.id < n2.id){
        return -1;
      }
      return 0;
    });
    this.updateVisiblePosts();
  }

  sortNewestFirst(){
    this.blogposts = this.blogposts.sort((n1, n2) => {
      if(n1.id < n2.id){
        return 1;
      }
      if(n1.id > n2.id){
        return -1;
      }
      return 0;
    });
    this.updateVisiblePosts();
  }

  sortAlphabeticalTitle(){
    this.blogposts = this.blogposts.sort((n1, n2) => {
      if(n1.title > n2.title){
        return 1;
      }
      if(n1.title < n2.title){
        return -1;
      }
      return 0;
    });
    this.updateVisiblePosts();
  }

  /*sortVisibleFirst(){
    let max = this.blogposts.length;
    let counter = 0;
    for (let i = 0; i < max; i++) {
      if(this.blogposts[counter].visible == false){
        this.blogposts.push(this.blogposts[counter]);
        this.blogposts.splice(counter, 1);
      }
      else
      {
        counter++;
      }
    }
  }*/

  addPost(titel:string, start:string, elementen: string, tekst:string[]){
    this.blogposts.push({
      id:this.blogposts.length+1,
      visible:true,
      title:titel,
      img:"../assets/img/catan.jpg",
      intro:start,
      tags:elementen.split(/\n/),
      text:tekst
    })
    this.updateVisiblePosts();
    return this.blogposts.length;
  }

  searchTitle(){
    var result = this.blogposts.filter(item => item.title.includes(this.searchterm));
    return result;
  }
  
  searchIntro(){
    var result = this.blogposts.filter(item => item.intro.includes(this.searchterm));
    return result;
  }

  

  

}



