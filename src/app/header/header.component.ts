import { Component, OnInit } from '@angular/core';
import { BlogpostsService } from '../blogposts.service';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { Router } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  data?; 
  input?: string
   
  constructor(public blogposts:BlogpostsService, private route:ActivatedRoute, private navigator:Router ) {
    
    this.data = blogposts
   }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.blogposts.searchterm = params["zoeken"],
      this.input = params["zoeken"];     
    })
  }

  onSubmit(){
    
    this.navigator.navigate(['search']);
  }

  login(){
    if (((<HTMLInputElement>document.getElementById("username")).value== this.blogposts.username) && ((<HTMLInputElement>document.getElementById("password")).value== this.blogposts.password))
    {
      this.blogposts.Loggedin = true;
    }
    
  }
  logout(){
    this.blogposts.Loggedin = false;
  }
  
 

}
