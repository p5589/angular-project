import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BlogpostsService } from '../blogposts.service';
import { HeaderComponent } from '../header/header.component';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  searchparameter: string ="oke";
  check?: string;
  titles;
  tags;
  intros;
  item?: string;
  
  
  constructor(private blogposts:BlogpostsService, private header:HeaderComponent, private route:ActivatedRoute,) { 
    this.searchparameter= this.blogposts.searchterm;
    this.check = this.header.input;
    this.titles = blogposts.blogposts;
    this.tags = blogposts.blogposts;
    this.intros = blogposts.blogposts;
  }

  ngOnInit(): void {
    
     this.titles = this.blogposts.searchTitle();
     
     this.intros = this.blogposts.searchIntro();
     
  }
  
  
  

}
