import { Component, OnInit } from '@angular/core';
import { BlogpostsService } from '../blogposts.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  posts?;
  loadedPosts = 0;
  maxPosts=10;
  activeFilters:string[] = [];
  isOldest = false;
  isNewest = false;
  isAlpha = false;
  constructor(public blogposts:BlogpostsService) {
    this.posts = blogposts.loadBlogPosts(3, this.loadedPosts);
    this.loadedPosts = 3;
    this.maxPosts = blogposts.blogposts.length;
    this.sortNewest();
   }

  ngOnInit(): void {
  }
  
  loadMorePosts(nr:number, reset:boolean=false){
    if(nr + this.loadedPosts > this.maxPosts)
    {
      nr = this.maxPosts - this.loadedPosts;
    }
    if(reset){
      this.posts = this.blogposts.loadBlogPosts(this.loadedPosts, 0);
    }
    else{
      this.posts = this.posts!.concat(this.blogposts.loadBlogPosts(nr, this.loadedPosts));
    }
    if(this.posts!.length > this.loadedPosts){
      this.loadedPosts = this.posts!.length;
    }
  }

  sort(){
    let sortBox = (document.getElementById("sort") as HTMLSelectElement);
    let sortType = sortBox.options[sortBox.selectedIndex].value;
    switch (sortType) {
      case "oldest":
        this.sortOldest();
        break;
      case "alpha":
        this.sortAlphabetical();
        break;
      case "newest":
        this.sortNewest();
        break;
    }
  }

  sortOldest(){
    this.blogposts.sortOldestFirst();
    this.loadMorePosts(0, true);
    this.isOldest = true;
    this.isNewest = false;
    this.isAlpha = false;
  }

  sortNewest(){
    this.blogposts.sortNewestFirst();
    this.loadMorePosts(0, true);
    this.isOldest = false;
    this.isNewest = true;
    this.isAlpha = false;
  }

  sortAlphabetical(){
    this.blogposts.sortAlphabeticalTitle();
    this.loadMorePosts(0, true);
    this.isOldest = false;
    this.isNewest = false;
    this.isAlpha = true;
  }

  filterPosts(){
    this.posts = [];
    this.blogposts.blogposts.forEach(post =>{
      post.visible = true;
    });
    if (this.activeFilters.length > 0)
    {
      this.blogposts.blogposts.forEach(post =>{
        this.activeFilters.forEach(filter => {
          if(!post.tags.find(x => x == filter)){
            post.visible = false;
          }
        });
      });
    }
    if (this.isOldest){
      this.sortOldest();
    }
    else if (this.isNewest){
      this.sortNewest();
    }
    else{
      this.sortAlphabetical();
    }
  }

  filter(filter:string){
    if (this.activeFilters.find(x => x == filter))
    {
      let index = this.activeFilters.indexOf(filter);
      this.activeFilters.splice(index, 1);
    }
    else
    {
      this.activeFilters.push(filter);
    }
    this.filterPosts();
  }

  resetFilters(){
    this.activeFilters=[];
    this.filterPosts();
  }


}
