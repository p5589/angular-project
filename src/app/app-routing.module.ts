import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { PostComponent } from './post/post.component';
import { SearchComponent } from './search/search.component';
import { AboutComponent } from './about/about.component';
import { NewPostComponent } from './new-post/new-post.component';
import { CanActivate } from '@angular/router';
import { CoastGuardsGuard } from './coast-guards.guard';

const routes: Routes = [
  {path:"", component: MainComponent},
  {path:"post", component: PostComponent},
  {path:"search", component: SearchComponent},
  {path:"about", component: AboutComponent},
  {path:"newpost", component: NewPostComponent, canActivate: [CoastGuardsGuard]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
